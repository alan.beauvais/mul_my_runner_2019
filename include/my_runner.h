/*
** EPITECH PROJECT, 2019
** MUL_my_runner_2019
** File description:
** my_runner
*/

#ifndef MY_RUNNER_H_
#define MY_RUNNER_H_

// CSFML
#include <SFML/Graphics.h>

#include "struct.h"

#define BG_PATH "res/pic/background.png"
#define GR_PATH "res/pic/ground.png"
#define MI_PATH "res/pic/middle.png"

#define GRAVITY 25

// game
void create_game(game_t *game);
void render_game(game_t *game);
void destroy_game(game_t *game);

// score_manager
void render_score(game_t *game, sfTime time);
void set_score(game_t *game, sfTime time);

// window_manager
window_t create_window(char *name, int width, int height);

// event_manager
void event_manager(game_t *game);

// object_manager
object_t *create_object(char *fp, sfVector2f pos, sfVector2f sp);
void draw_obj(game_t *game, object_t *object);
void move_object(object_t *object, float delta);
void destroy_obj(object_t *object);

// parallax_manager
void move_parallax(object_t *object, sfTime time, game_t *game, float size);
void render_parallax(game_t *game, sfTime time);
void create_parallax(game_t *game);

// player_manager
player_t *create_player(void);
void render_play(game_t *game, sfTime time);
void draw_player(sfRenderWindow *window, player_t *player);
void move_rect(float *elapsed_time, sfIntRect *rect);

// jumping_manager
void jumping_player(game_t *game);

#endif /* !MY_RUNNER_H_ */
