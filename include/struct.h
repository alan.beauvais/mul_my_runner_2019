/*
** EPITECH PROJECT, 2019
** MUL_my_runner_2019
** File description:
** struct
*/

#ifndef STRUCT_H_
#define STRUCT_H_

#include <SFML/Graphics.h>
#include <SFML/Audio.h>

typedef struct window
{
    char *name;
    sfRenderWindow *window;
    sfVideoMode mode;
    sfEvent event;
} window_t;

typedef struct player
{
    sfSprite *sprite;
    sfTexture *texture;
    sfVector2f position;
    sfVector2f speed;
    sfIntRect rect;
    sfSound *sound;
    sfSoundBuffer *sound_buf;
    int is_grounded;
} player_t;

typedef struct object
{
    sfSprite *sprite;
    sfTexture *texture;
    sfVector2f position;
    sfVector2f scale;
    sfVector2f speed;
} object_t;

typedef struct game
{
    window_t window;
    player_t *player;
    object_t *background;
    object_t *middle;
    object_t *ground;
    sfClock *clock;
    sfMusic *music;
    sfFont *font;
    sfText *text;
    float elapsed_time;
    int score;
    float gravity;
} game_t;


#endif /* !STRUCT_H_ */