/*
** EPITECH PROJECT, 2019
** struct.h
** File description:
** struct
*/

#include <stdarg.h>
#include "my.h"

#ifndef STRUCT_H_
#define STRUCT_H_

typedef struct flag_h
{
    int id;
    char flag;
} flag_help_t;


struct flag
{
    char flag;
    int (*function)(va_list list);
};

#endif /* !STRUCT_H_ */
