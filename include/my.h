/*
** EPITECH PROJECT, 2019
** MUL_my_runner_2019
** File description:
** my
*/

#ifndef MY_H_
#define MY_H_

#define M_HELPER 1

int my_putstr(char const *str);
int print_helper(char *name);
int is_flag(char *param, int id);
char *set_number(int nbr);

#endif
