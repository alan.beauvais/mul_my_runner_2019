/*
** EPITECH PROJECT, 2020
** MUL_my_runner_2019
** File description:
** game
*/

#include <SFML/Graphics.h>
#include <SFML/Audio.h>
#include "my.h"
#include "my_runner.h"
#include "struct.h"

const char *MUSIC_PATH = "res/music/Animaniac.ogg";
const char *FONT_PATH = "res/font/PressStart2P-Regular.ttf";

void create_game(game_t *game)
{
    create_parallax(game);
    game->score = 0;
    game->clock = sfClock_create();
    game->elapsed_time = 0;
    game->window = create_window("Little Boy", 1280, 720);
    game->player = create_player();
    game->music = sfMusic_createFromFile(MUSIC_PATH);
    game->text = sfText_create();
    game->font = sfFont_createFromFile(FONT_PATH);
    game->gravity = GRAVITY;
    sfText_setFont(game->text, game->font);
    sfText_setPosition(game->text, (sfVector2f){50, 20});
    sfMusic_setLoop(game->music, sfTrue);
}

void render_game(game_t *game)
{
    sfTime time;

    sfRenderWindow_setFramerateLimit(game->window.window, 60);
    sfMusic_play(game->music);
    while (sfRenderWindow_isOpen(game->window.window)) {
        event_manager(game);
        sfRenderWindow_clear(game->window.window, sfColor_fromRGB(0, 0, 0));
        time = sfClock_getElapsedTime(game->clock);
        sfClock_restart(game->clock);
        jumping_player(game);
        printf("%d\n", game->gravity);
        render_parallax(game, time);
        render_play(game, time);
        render_score(game, time);
        sfRenderWindow_display(game->window.window);
    }
    sfMusic_stop(game->music);
    sfRenderWindow_destroy(game->window.window);
}

void destroy_game(game_t *game)
{
    destroy_obj(game->background);
    destroy_obj(game->ground);
    sfText_destroy(game->text);
    sfFont_destroy(game->font);
    sfMusic_destroy(game->music);
    sfSound_destroy(game->player->sound);
    sfSoundBuffer_destroy(game->player->sound_buf);
    sfClock_destroy(game->clock);
}