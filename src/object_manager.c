/*
** EPITECH PROJECT, 2020
** MUL_my_runner_2019
** File description:
** object_manager
*/

#include <stdlib.h>
#include "struct.h"

object_t *create_object(char *fp, sfVector2f pos, sfVector2f sp)
{
    object_t *object = malloc(sizeof(object_t));

    object->speed = sp;
    object->position = pos;
    object->texture = sfTexture_createFromFile(fp, NULL);
    object->sprite = sfSprite_create();
    sfSprite_setTexture(object->sprite, object->texture, sfFalse);
    sfSprite_setPosition(object->sprite, pos);
    return object;
}

void move_object(object_t *object, float delta)
{
    object->position.x -= (object->speed.x * delta);
    sfSprite_setPosition(object->sprite, object->position);
}

void draw_obj(game_t *game, object_t *object)
{
    sfRenderWindow_drawSprite(game->window.window, object->sprite, NULL);
}

void destroy_obj(object_t *object)
{
    sfSprite_destroy(object->sprite);
    sfTexture_destroy(object->texture);
}