/*
** EPITECH PROJECT, 2020
** MUL_my_runner_2019
** File description:
** jumping_manager
*/

#include <SFML/Graphics.h>
#include "struct.h"
#include "my_runner.h"

void jumping_player(game_t *game)
{
    if (!(game->player->is_grounded)) {
        game->gravity++;
        game->player->position.y += game->gravity;
    }
    if (game->player->position.y >= 475) {
        game->player->is_grounded = 1;
        game->gravity = GRAVITY;
    }
    sfSprite_setPosition(game->player->sprite, game->player->position);
}