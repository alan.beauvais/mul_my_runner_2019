/*
** EPITECH PROJECT, 2020
** MUL_my_runner_2019
** File description:
** main
*/

#include "my.h"
#include "my_runner.h"
#include "struct.h"

int main(int ac, char **av)
{
    game_t game;

    if (ac == 2 && is_flag(av[1], M_HELPER))
        return print_helper(av[0]);
    create_game(&game);
    render_game(&game);
    destroy_game(&game);
    return 0;
}