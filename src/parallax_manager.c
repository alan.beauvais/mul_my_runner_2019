/*
** EPITECH PROJECT, 2020
** MUL_my_runner_2019
** File description:
** parallax
*/

#include <SFML/Graphics.h>
#include "struct.h"
#include "my_runner.h"

void create_parallax(game_t *game)
{
    sfVector2f b_speed = {60, 0};
    sfVector2f b_pos = {0, 0};
    sfVector2f m_speed = {100, 0};
    sfVector2f m_pos = {0, -55};
    sfVector2f g_speed = {150, 0};
    sfVector2f g_pos = {0, 600};

    game->background = create_object(BG_PATH, b_pos, b_speed);
    game->middle = create_object(MI_PATH, m_pos, m_speed);
    game->ground = create_object(GR_PATH, g_pos, g_speed);
}

void move_parallax(object_t *object, sfTime time, game_t *game, float size)
{
    object->position.x += size;
    move_object(object, sfTime_asSeconds(time));
    draw_obj(game, object);
    if (object->position.x <= 0)
        object->position.x += size;
    object->position.x -= size;
    move_object(object, sfTime_asSeconds(time));
    draw_obj(game, object);
}

void render_parallax(game_t *game, sfTime time)
{

    move_parallax(game->background, time, game, 1885);
    move_parallax(game->middle, time, game, 2117);
    move_parallax(game->ground, time, game, 1940);
    game->elapsed_time += sfTime_asSeconds(time);
}