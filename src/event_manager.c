/*
** EPITECH PROJECT, 2020
** MUL_my_runner_2019
** File description:
** event_manager
*/

#include <SFML/Graphics.h>
#include "struct.h"
#include "my_runner.h"

void keyboard_manager(game_t *game, sfKeyEvent key)
{
    if (game->player->is_grounded && key.code == sfKeySpace) {
        game->gravity *= -1;
        game->player->is_grounded = 0;
        sfSound_play(game->player->sound);
    }
}

void event_manager(game_t *game)
{
    window_t my_win = game->window;

    while (sfRenderWindow_pollEvent(my_win.window, &my_win.event)) {
        if (my_win.event.type == sfEvtClosed)
            sfRenderWindow_close(my_win.window);
        if (my_win.event.type == sfEvtKeyPressed)
            keyboard_manager(game, my_win.event.key);
    }
}
