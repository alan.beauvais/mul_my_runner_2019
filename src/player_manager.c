/*
** EPITECH PROJECT, 2020
** MUL_my_runner_2019
** File description:
** player_manager
*/

// CSFML
#include <SFML/Graphics.h>

// util
#include <stdlib.h>

// My include
#include "my.h"
#include "my_runner.h"
#include "struct.h"

const char *JUMP_PATH = "res/music/jump.ogg";
const char *SPRITE_PATH = "res/pic/player_run.png";

player_t *create_player(void)
{
    player_t *player = malloc(sizeof(player_t));

    player->rect = (sfIntRect){0, 0, 53, 61};
    player->position = (sfVector2f){150, 475};
    player->texture = sfTexture_createFromFile(SPRITE_PATH, NULL);
    player->sound_buf = sfSoundBuffer_createFromFile(JUMP_PATH);
    player->sprite = sfSprite_create();
    player->sound = sfSound_create();
    player->is_grounded = 1;
    sfSprite_setTexture(player->sprite, player->texture, sfFalse);
    sfSprite_setScale(player->sprite, (sfVector2f){2.25, 2.25});
    sfSprite_setPosition(player->sprite, player->position);
    sfSound_setBuffer(player->sound, player->sound_buf);
    return player;
}

void render_play(game_t *game, sfTime time)
{
    sfSprite_setTextureRect(game->player->sprite, game->player->rect);
    draw_player(game->window.window, game->player);
    game->elapsed_time += sfTime_asSeconds(time);
    move_rect(&game->elapsed_time, &game->player->rect);
}

void draw_player(sfRenderWindow *window, player_t *player)
{
    sfRenderWindow_drawSprite(window, player->sprite, NULL);
}

void move_rect(float *elapsed_time, sfIntRect *rect)
{
    static int rec_size;

    rec_size = rect->width;
    if (*elapsed_time * .75f > 0.1) {
        if (rect->left >= rec_size * 7) {
            rect->left = 0;
        }
        else
            rect->left += rec_size;
        *elapsed_time = 0;
    }
}
