/*
** EPITECH PROJECT, 2020
** MUL_my_runner_2019
** File description:
** window_manager
*/

#include "struct.h"

window_t create_window(char *name, int width, int height)
{
    window_t win;

    win.name = name;
    win.mode.width = width;
    win.mode.height = height;
    win.mode.bitsPerPixel = 32;
    win.window = sfRenderWindow_create(win.mode, name, sfDefaultStyle, NULL);
    return win;
}
