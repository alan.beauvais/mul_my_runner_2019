/*
** EPITECH PROJECT, 2020
** MUL_my_runner_2019
** File description:
** score_manager
*/

#include <SFML/Graphics.h>
#include "my_runner.h"
#include "struct.h"
#include "my.h"

void render_score(game_t *game, sfTime time)
{
    set_score(game, time);
    sfText_setString(game->text, set_number(game->score));
    sfRenderWindow_drawText(game->window.window, game->text, NULL);
}

void set_score(game_t *game, sfTime time)
{
    static float elapsed_time;

    elapsed_time += sfTime_asSeconds(time);
    if (elapsed_time * .5 > .1) {
        game->score++;
        elapsed_time = 0;
    }
}