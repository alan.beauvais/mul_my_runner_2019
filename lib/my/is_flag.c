/*
** EPITECH PROJECT, 2020
** libmy
** File description:
** is_flag.c
*/

#include "lib_struct.h"

const flag_help_t flags[] = {
    {1, 'h'},
    {2, 'c'}
};

int get_flag(int id)
{
    int i = 0;

    while (flags[i].id) {
        if (flags[i].id == id)
            return i;
    }
    return -1;
}

int is_flag(char *param, int id)
{
    int flag = get_flag(id);

    if (flag > 0)
        return 0;
    else if (param[1] == flags[flag].flag)
        return 1;
    else
        return 0;
}
