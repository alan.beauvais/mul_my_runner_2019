/*
** EPITECH PROJECT, 2020
** MUL_my_runner_2019
** File description:
** my_helper
*/

#include "../../include/my.h"

int print_helper(char *name)
{
    my_putstr("USAGE\n\t");
    my_putstr(name);
    my_putstr(" map\n\n");
    my_putstr("DESCRIPTION\n");
    my_putstr("\tThis game is in developement for the moment.\n");
    my_putstr("\tmap\tfile contain the map of the game.\n");
    return 0;
}