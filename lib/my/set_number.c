/*
** EPITECH PROJECT, 2020
** MUL_my_runner_2019
** File description:
** int_str
*/

#include <stdlib.h>

int int_len(int nbr)
{
    int len = 0;

    if (nbr < 0)
        return 0;
    len++;
    while (nbr > 9) {
        nbr /= 10;
        len++;
    }
    return len;
}

char *set_number(int nbr)
{
    int buffer;
    int number_len = int_len(nbr);
    char *str = malloc(sizeof(char) * number_len + 1);

    for (int i = number_len - 1; i >= 0; i--) {
        buffer = nbr % 10;
        nbr /= 10;
        str[i] = buffer + '0';
    }
    str[number_len] = '\0';
    return str;
}