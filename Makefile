##
## EPITECH PROJECT, 2019
## Makefile
## File description:
## My makefile for the second rush
##

SRC =	./src/main.c \
		./src/game.c \
		./src/window_manager.c \
		./src/event_manager.c \
		./src/object_manager.c \
		./src/parallax_manager.c \
		./src/player_manager.c \
		./src/score_manager.c \
		./src/jumping_manager.c

OBJ =	$(SRC:.c=.o)

NAME =	my_runner
CFLAGS = -Wall -Wextra -I./include/ -g
CSFML =	-l csfml-audio -l csfml-graphics -l csfml-system -l csfml-window -l csfml-network

all :	$(NAME)

$(NAME) :	$(OBJ)
	make -C lib/my
	gcc -o $(NAME) $(OBJ) -L./lib -lmy $(CSFML)

clean :
	rm -f $(OBJ)
	make clean -C lib/my
	rm -f ~* \#*\#

fclean :	clean
	rm -f $(NAME)
	make fclean -C lib/my

re :	fclean all
